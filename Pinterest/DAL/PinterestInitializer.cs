﻿using Pinterest.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pinterest.DAL
{
    public class PinterestInitializer : System.Data.Entity.DropCreateDatabaseIfModelChanges<PinterestContext>
    {
        protected override void Seed(PinterestContext context)
        {
            var users = new List<User>()
            {
                new User() { Nickname="anna", Location="Dublin", Email="anna@fesb.hr", UserPhoto="slika" },
                new User() { Nickname="John", Location="Amsterdam", Email="john@fesb.hr", UserPhoto="slika" }
            };
            users.ForEach(s => context.Users.Add(s));
            context.SaveChanges();

            var tags = new List<Tag>()
            {
                new Tag() { TagName = "Animals and pets"},
                new Tag() { TagName = "DIY and craft" },
                new Tag() { TagName = "Films, music and books" }
            };
            tags.ForEach(s => context.Tags.Add(s));
            context.SaveChanges();

            var comments = new List<Comment>()
            {
                new Comment() { Text = "kom", Vrijeme = DateTime.Now, User = users.ElementAt(0)},
                new Comment() { Text = "kome", Vrijeme = DateTime.Now,User = users.ElementAt(1)},
                new Comment() { Text = "komen", Vrijeme = DateTime.Now, User = users.ElementAt(0)},
                new Comment() { Text = "koment", Vrijeme = DateTime.Now, User = users.ElementAt(0)}
            };
            comments.ForEach(s => context.Comments.Add(s));
            context.SaveChanges();

            var pins = new List<Pin>()
            {
                new Pin() { Title="Bernese Mountain Dogs", Text = "They are absolutely adorable!", Picture="https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcRMnoLCElHpVXZAq4LsWPrUOklrI1fZpdKa6Mm-0lZVq65Q8jOFQg", Tag="Animals and pets", UserCreator = users.ElementAt(0),Comments= new List<Comment> { comments.ElementAt(0)}},
                new Pin() { Title="DIY", Text = "nesto", Picture="https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcT4sIkACqM7ySMvyx1xaEUuHl0BGcujK6jGclYALgOGOj4WKA9xXg", Tag = "DIY and crafts",  UserCreator = users.ElementAt(0), Comments= new List<Comment> { comments.ElementAt(1),comments.ElementAt(3)} },
                new Pin() { Title="Stranger Things", Text = "I love this show!", Picture="https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcR4u3coCkTbMItIs_OCGL7TXFlXTD8zF_V4mBSaN44plDGXF5dmAw", Tag = "Films, music and books", UserCreator = users.ElementAt(1),Comments= new List<Comment> { comments.ElementAt(2) } }
            };
            pins.ForEach(s => context.Pins.Add(s));
            context.SaveChanges();

            var likes = new List<Like>()
            {
                new Like() { Pin = pins.ElementAt(0), Value = true },
                new Like() { Pin = pins.ElementAt(0), Value = false },
                new Like() { Pin = pins.ElementAt(1), Value = true },
            };
            likes.ForEach(s => context.Likes.Add(s));
            context.SaveChanges();
        }
    }
}